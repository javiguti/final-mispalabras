# Entrega practica
## Datos
* Nombre: Javier Gutiérrez Rodríguez
* Titulación: Ingeniería en sistemas de la telecomunicación
* Despliegue (url): http://javiguti.pythonanywhere.com/
* Video básico (url): https://www.youtube.com/watch?v=AgVoVtTLwc8
## Cuenta Admin Site
* javiguti/pythonweb 
## Cuentas usuarios
* javiguti/pythonweb
* martin/pythonweb
* pepe/pythonweb
## Resumen parte obligatoria
* Descargo imagenes en ficheros para los memes.
* En modo local mi página se arranca en localhost:1234 usando python3 manage.py runserver "127.0.0.1":1234  pudiendo cambiar los numeros de delante de los dos puntos que a mi no me iba el puerto que da por defecto y no quería modificar la práctica ccon ese puerto por si acaso luego no funcionaba y no podía comprobarlo.
* En definición Rae si no carga la página se guarda esta definición no se encuentra en la RAE
## Lista partes opcionales
* Nombre parte: Revertir boton de votar
