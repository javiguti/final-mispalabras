from django.test import TestCase
from django.test import Client
from django.contrib.auth.models import User

from .models import Usuario, Page


class GetTests (TestCase):

    def test_get_resources(self):

        ## Inicio
        c = Client()
        response = c.get('/')
        self.assertEqual(response.status_code, 200)
        content = response.content.decode('utf-8')
        self.assertIn('<header><h2>Mis palabras</h2></header>', content)
        ## JSON Inicializar server para ejecutar
        c = Client()
        response = c.get('/?format=json')
        self.assertEqual(response.status_code, 200)
        content = response.content.decode('utf-8')
        self.assertIn('<h3>Contenido</h3>', content)
        ## XML Inicializar server para ejecutar
        c = Client()
        response = c.get('/?format=xml')
        self.assertEqual(response.status_code, 200)
        content = response.content.decode('utf-8')
        self.assertIn('<h3>Contenido</h3>', content)
        ## Login
        c = Client()
        response = c.get('/login/')
        self.assertEqual(response.status_code, 200)
        content = response.content.decode('utf-8')
        self.assertIn('Contraseña', content)
        ## Get item
        c = Client()
        response = c.get('/pala')
        self.assertEqual(response.status_code, 200)
        content = response.content.decode('utf-8')
        self.assertIn('Main page', content)
        ## Ayuda
        c = Client()
        response = c.get('/ayuda/')
        self.assertEqual(response.status_code, 200)
        content = response.content.decode('utf-8')
        self.assertIn('Autor: Javier Gutiérrez Rodríguez', content)
        ## Mi pagina
        test_user1 = User.objects.create_user(username='testuser1', password='12345')
        test_user1.save()
        test_usuario = Usuario(id=test_user1.id, name=test_user1.username)
        test_usuario.save()
        login = self.client.login(username='testuser1', password='12345')
        response = self.client.get('/mi-pagina/')
        self.assertEqual(response.status_code, 200)
        ## Logout
        response = c.get('/logout/')
        self.assertEqual(response.status_code, 302) ## Redirección

    def test_post_resources(self):

        ## Buscador de pablabra
        item = "item"
        c = Client()
        response = c.post('/', {'content': item})
        self.assertEqual(response.status_code, 200)
        test_user1 = User.objects.create_user(username='testuser1', password='12345')
        test_user1.save()
        ## Rellenar formulario de un objeto
        test_usuario = Usuario(id=test_user1.id, name=test_user1.username)
        test_usuario.save()
        login = self.client.login(username='testuser1', password='12345')
        response = self.client.post('/pala', {'content': item})
        self.assertEqual(response.status_code, 200)
        ## Rellenar formulario de un objeto 2
        test_usuario = Usuario(id=test_user1.id, name=test_user1.username)
        test_usuario.save()
        login = self.client.login(username='testuser1', password='12345')
        response = self.client.post('/pala', {'name': 'pala', 'select': True})
        self.assertEqual(response.status_code, 200)
        ## Rellenar formulario de un objeto 3
        test_page = Page(name='palabra', content='definicion', selected=False, almacenado=False)
        test_page.save()
        login = self.client.login(username='testuser1', password='12345')
        response = self.client.post('/palabra', {'name': 'palabra', 'almacenar': 'almacenar'})
        self.assertEqual(response.status_code, 200)
        ## Rellenar formulario de un objeto 4
        login = self.client.login(username='testuser1', password='12345')
        response = self.client.post('/pala', {'comment': 'comment', 'content': 'content'})
        self.assertEqual(response.status_code, 200)
        ## Rellenar formulario de un objeto 5
        login = self.client.login(username='testuser1', password='12345')
        response = self.client.post('/pala', {'url': 'url', 'buscador': 'buscador'})
        self.assertEqual(response.status_code, 200)
        ## Rellenar formulario de un objeto 6
        login = self.client.login(username='testuser1', password='12345')
        response = self.client.post('/pala', {'RaeDef': 'RaeDef'})
        self.assertEqual(response.status_code, 200)
        ## Rellenar formulario de un objeto 7
        login = self.client.login(username='testuser1', password='12345')
        response = self.client.post('/pala', {'Flickr': 'Flickr'})
        self.assertEqual(response.status_code, 200)
        ## Rellenar formulario de un objeto 8
        login = self.client.login(username='testuser1', password='12345')
        response = self.client.post('/pala', {'Meme': 'Meme', 'opcion1': 'opcion1'})
        self.assertEqual(response.status_code, 200)
        ## Rellenar formulario de un objeto 9
        login = self.client.login(username='testuser1', password='12345')
        response = self.client.post('/pala', {'Meme': 'Meme', 'opcion2': 'opcion2'})
        self.assertEqual(response.status_code, 200)
        ## Post login
        c = Client()
        response = c.post('/login/', {'nombre': 'javiguti', 'contrasena': 'pythonweb'})
        response.status_code
        ## Votar inicio
        login = self.client.login(username='testuser1', password='12345')
        response = self.client.post('/', {'name': 'pala', 'select': True})
        self.assertEqual(response.status_code, 200)

