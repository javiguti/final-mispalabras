# Generated by Django 4.0.3 on 2022-05-29 14:14

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('practica_final', '0005_url_info_url_link_image'),
    ]

    operations = [
        migrations.RenameField(
            model_name='page',
            old_name='almacenado',
            new_name='saved',
        ),
    ]
