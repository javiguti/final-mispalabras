import urllib.request

from django.db import migrations
from ..apps import WikipDef

def descarga_lista_palabras(apps, schema_editor):
    Page = apps.get_model('practica_final', 'Page')
    lista_palabra = ['meme', 'pala', 'alfombra', 'villa', 'playa', 'azotea', 'ajedrez','puerta', 'llave', 'diana']
    for palabra in lista_palabra:
        url = 'https://es.wikipedia.org/w/api.php?action=query&format=xml&titles='+ palabra + \
          '&prop=extracts&exintro&explaintext'
        xmlStream = urllib.request.urlopen(url)
        definicion = WikipDef(xmlStream).definition()
        p = Page(name=palabra, content=definicion, selected=False, almacenado=True)
        p.save()

class Migration(migrations.Migration):

    dependencies = [
        ('practica_final', '0001_initial'),
    ]

    operations = [
        migrations.RunPython(descarga_lista_palabras)
    ]