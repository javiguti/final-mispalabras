# Generated by Django 4.0.3 on 2022-05-28 16:52

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('practica_final', '0003_rename_comment_page_comment2_rename_url_page_url2_and_more'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='page',
            name='comment2',
        ),
        migrations.RemoveField(
            model_name='page',
            name='url2',
        ),
    ]
