from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('logout/', views.logout_view, name='logout_view'),
    path('login/', views.login_view, name='login_view'),
    path('<name>', views.word_content, name='word__content'),
    path('ayuda/', views.help, name='help'),
    path('mi-pagina/', views.my_page, name='my_page'),
]