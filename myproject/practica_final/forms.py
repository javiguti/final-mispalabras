from django import forms

class ContentForm(forms.Form):
    content = forms.CharField(label='Comenta', widget=forms.Textarea)

class BuscadorForm(forms.Form):
    buscador = forms.CharField(label='Buscador', max_length=100)
