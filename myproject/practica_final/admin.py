from django.contrib import admin
from .models import Page, Voto, Usuario

admin.site.register(Page)
admin.site.register(Voto)
admin.site.register(Usuario)