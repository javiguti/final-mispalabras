from django.db import models

# Create your models here.
class Usuario(models.Model):
    id = models.CharField(max_length=50, primary_key=True)
    name = models.CharField(max_length=30)


class Page(models.Model):
    name = models.CharField(max_length=256)
    content = models.CharField(max_length=1024)
    def_rae = models.CharField(max_length=1024, default='')
    selected = models.BooleanField(default=False)
    almacenado = models.BooleanField(default=True)
    rae_alm = models.BooleanField(default=False)
    fl_alm = models.BooleanField(default=False)
    mem_alm = models.BooleanField(default=False)
    author = models.ForeignKey(Usuario, null=True, blank=True, on_delete=models.CASCADE)
    date = models.DateTimeField(null=True, blank=True)

class Voto(models.Model):
    word = models.ForeignKey(Page, null=True, blank=True, on_delete=models.CASCADE)
    author = models.ForeignKey(Usuario, null=True, blank=True, on_delete=models.CASCADE)

class Comentario(models.Model):
    word = models.ForeignKey(Page, null=True, blank=True, on_delete=models.CASCADE)
    author = models.ForeignKey(Usuario, null=True, blank=True, on_delete=models.CASCADE)
    comment = models.TextField(default='')
    date = models.DateTimeField(null=True, blank=True)

class Url(models.Model):
    word = models.ForeignKey(Page, null=True, blank=True, on_delete=models.CASCADE)
    author = models.ForeignKey(Usuario, null=True, blank=True, on_delete=models.CASCADE)
    url = models.TextField(default='')
    info = models.TextField(default='')
    link_image = models.TextField(default='')
    date = models.DateTimeField(null=True, blank=True)
