import math
from django.core.paginator import Paginator
from django.http import HttpResponse
from django.template import loader
from django.views.decorators.csrf import csrf_exempt
from django.shortcuts import render, redirect
from django.contrib.auth import logout, authenticate, login
from .forms import ContentForm, BuscadorForm
from .models import Page, Usuario, Voto, Comentario, Url
from .apps import WikipDef, FlickrImg
import urllib.request, json
from bs4 import BeautifulSoup
import xml.etree.cElementTree as Tree
import requests
import random
import datetime

## Apartados de descarga
def download_word(request, name):
    user = Usuario.objects.get(id=request.user.id)
    try:
        url = 'https://es.wikipedia.org/w/api.php?action=query&format=xml&titles='+ name + \
          '&prop=extracts&exintro&explaintext'
        xmlStream = urllib.request.urlopen(url)
        definition = WikipDef(xmlStream).definition()
    except NameError:
        definition = 'No hay definicion en Wikipedia en español.'
    p = Page(name=name, content=definition, selected=False, almacenado=False, author=user)
    p.save()


def download_meme(name, opt):
    f = open('practica_final/static/pages/imagenes/' + name + '.jpg', 'wb')
    print(opt)
    if opt == 1:
        response = requests.get('http://apimeme.com/meme?meme=Afraid-To-Ask-Andy&top=' + name + '&bottom=%C2%BFo+no%3F')
    elif opt == 2:
        response = requests.get('http://apimeme.com/meme?meme=1990s-First-World-Problems&top=' + name + \
                                '&bottom=Good')
    elif opt == 3:
        response = requests.get('http://apimeme.com/meme?meme=Advice-Dog&top=' + name + '&bottom=Wow')
    f.write(response.content)
    f.close()


def download_image_wiki(name):
    url = 'https://es.wikipedia.org/w/api.php?action=query&titles=' + name + \
          '&prop=pageimages&format=json&pithumbsize=200'
    with urllib.request.urlopen(url) as json_doc:
        json_str = json_doc.read().decode(encoding="ISO-8859-1")
        archivo = json.loads(json_str)
    image = archivo['query']['pages']
    id = list(image.keys())
    try:
        image = image[str(id[0])]['thumbnail']
        source = image['source']
        height = image['height']
        width = image['width']
        answer = 'Esta es la imagen que ofrece wikipedia de ' + name + ':'
    except:
        source = ''
        height = ''
        width = ''
        answer = 'Esta palabra no tiene imagen en Wikipedia en español.'
    return source, height, width, answer


def download_image_flickr(name):
    url = 'https://www.flickr.com/services/feeds/photos_public.gne?tags=' + name
    xmlStream = urllib.request.urlopen(url)
    link = FlickrImg(xmlStream).download_link()
    return link


def download_rae_def(name):
    try:
        url = 'https://dle.rae.es/' + name
        user_agent = "Mozilla/5.0 (X11) Gecko/20100101 Firefox/100.0"
        headers = {'User-Agent': user_agent}
        req = urllib.request.Request(url, headers=headers)
        with urllib.request.urlopen(req) as response:
            htmlStream = response.read().decode('utf8')
        soup = BeautifulSoup(htmlStream, 'html.parser')
        print("Title:", soup.title.string)
        ogDescription = soup.find('meta', property='og:description')
        if ogDescription:
            definition = "Definición RAE:" + ogDescription['content']
        ogImage = soup.find('meta', property='og:image')
        if ogImage:
            print("Image (og:image):", ogImage['content'])
    except:
        definition = 'No hay definición rae para esta palabra.'
    p = Page.objects.get(name=name)
    p.def_rae = definition
    p.save()

def download_url_emb(name, url):
    try:
        link = url.url + name
        user_agent = "Mozilla/5.0 (X11) Gecko/20100101 Firefox/100.0"
        headers = {'User-Agent': user_agent}
        req = urllib.request.Request(link, headers=headers)
        with urllib.request.urlopen(req) as response:
            htmlStream = response.read().decode('utf8')
        soup = BeautifulSoup(htmlStream, 'html.parser')
        print("Title:", soup.title.string)
        ogDescription = soup.find('meta', property='og:description')
        if ogDescription:
            definicion = "Definición URL:" + ogDescription['content']
        ogImage = soup.find('meta', property='og:image')
        if ogImage:
            print("Image (og:image):", ogImage['content'])
        url.info = definicion
        url.link_image = ogImage['content']
        url.save()
    except:
        url.info = ''
        url.save()


## Apartado modificación, búsqueda y recuento de palabras
def change_word(request, name, selected):
    p = Page.objects.get(name=name)
    user = Usuario.objects.get(id=request.user.id)
    if selected:
        v = Voto.objects.create(word=p, author=user)
        v.save()
    else:
        try:
            v = Voto.objects.get(word=p, author=user)
            v.delete()
        except:
            print('No hay voto.')
    p.selected=selected
    p.save()

def search_word(name):
    url = 'https://es.wikipedia.org/w/api.php?action=query&format=xml&titles='+ name + \
          '&prop=extracts&exintro&explaintext'
    xmlStream = urllib.request.urlopen(url)
    definition = WikipDef(xmlStream).definition()
    p = Page(name=name, content=definition, selected=False)
    p.save()

def word_recount():
    try:
        items = Page.objects.all()
        cont = 0
        items_names = []
        for item in items:
            if item.almacenado:
                items_names.append(item.name)
                cont =cont + 1
        p = items_names[random.randint(0, cont - 1)]
    except Page.DoesNotExist:
        cont = 0
        p = ""
    except TypeError:
        cont = 1
        p = items.name
    if cont < 10:
        cont = str(cont)
        cont = "00" + cont
    elif cont < 100:
        cont = str(cont)
        cont = "0" + cont
    return cont, p


def order_wordlist():
    items = Page.objects.all()
    items_names = []
    list_pages = []
    for item in items:
        if item.almacenado:
            items_names.append(item.name)
    items_names = reversed(items_names)
    for name in items_names:
        p = Page.objects.get(name=name)
        list_pages.append(p)
    return list_pages


## Apartado para la votación
def voto_recount(palabra):
    try:
        v = Voto.objects.filter(word=palabra)
        cont = len(v)
    except Voto.DoesNotExist:
        cont = 0
    except TypeError:
        cont = 1
    return cont

def load_voto(request):
    user = Usuario.objects.get(id=request.user.id)
    pages = Page.objects.all()
    for p in pages:
        v = Voto.objects.filter(word=p, author=user)
        if len(v) >= 1:
            p.selected = True
        else:
            p.selected = False
        p.save()


## Apartado Barra de Tareas
def taskbar(request):
    if request.user.is_authenticated:
        link = "http://localhost:1234/logout/"
        action = "Logout"
        verify = True
        try:
            user = Usuario.objects.get(id=request.user.id)
        except Usuario.DoesNotExist:
            user = Usuario(id=request.user.id, name=request.user.username)
            user.save()
        id = user.name
    else:
        link = "http://localhost:1234/login/"
        id = ''
        action = "Login"
        verify = False
    return link, action, verify, id

@csrf_exempt
def load_taskbar(request):
    items = Page.objects.all()
    video_list = {}
    for item in items:
        try:
            videos_votados = Voto.objects.filter(word=item)
            cont = len(videos_votados)
            video_list[item.name] = cont
        except Voto.DoesNotExist:
            cont = 0
            video_list[item.name] = cont
        except TypeError:
            cont = 1
            video_list[item.name] = cont
    ord_list = sorted(video_list.items(), key=lambda x: x[1], reverse=True)
    vote_list = []
    num = 0
    while (num < 10) and not (num >= len(ord_list)):
        p = Page.objects.get(name=ord_list[num][0])
        vote_list.append(p)
        num += 1
    content_form = BuscadorForm()
    items = order_wordlist()
    paginator = Paginator(items, 5)

    page_number = request.GET.get('page')
    item_list = paginator.get_page(page_number)
    link, action, verify, id = taskbar(request)
    return vote_list, content_form, item_list, link, id, action, verify


## Apartados contenidos json y xml
@csrf_exempt
def content_xml(request):
    res, book_no = [], 1
    try:
        items = Page.objects.all()
        cont = 0
        for item in items:
            if item.almacenado:
                cont = cont + 1
    except TypeError:
        cont = 1
    cont = cont / 5
    cont = math.ceil(cont)
    for i in range(1, cont + 1):
        url = 'http://localhost:1234/?page=' + str(i)
        with urllib.request.urlopen(url) as response:
            htmlStream = response.read().decode('utf8')
        soup = BeautifulSoup(htmlStream, 'html.parser')
        books = soup.find_all(
            'ol')
        for book in books:
            link = 'http://localhost:1234/' + book.find('a')['href']
            palabra = book.find('input', attrs={'name': 'name'})['value']
            token = book.find('input', attrs={'name': 'csrfmiddlewaretoken'})['value']
            data = {'book no': str(book_no), 'title': palabra, 'link': link,
                    'token': token}
            book_no += 1
            res.append(data)
    l = Tree.Element("Lista")
    for element in res:
        r = Tree.SubElement(l, "Palabra")
        Tree.SubElement(r, "Bookno").text = element['book no']
        Tree.SubElement(r, "Title").text = element["title"]
        Tree.SubElement(r, 'Link').text = str(element['link'])
        Tree.SubElement(r, 'Token').text = str(element['token'])
    a = Tree.ElementTree(l)
    a.write("practica_final/templates/pages/json_to_xml.xml")
    file = open('practica_final/templates/pages/json_to_xml.xml', 'r')
    text = file.read() + '?format=xml'
    link, accion, verify, id = taskbar(request)
    content_template = loader.get_template('pages/contenido_jsonorxml.html')
    cont_pags, pag_ej = word_recount()
    xml = True
    content_html = content_template.render( {
        'link': link,
        'id': id,
        'accion': accion,
        'autentificacion': verify,
        'datos': text,
         'cont_pags': cont_pags,
         'pag_ej': pag_ej,
         'xml': xml}, request)
    return (HttpResponse(content_html, status=200))


@csrf_exempt
def content_json(request):
    res, book_no = [], 1
    try:
        items = Page.objects.all()
        cont = 0
        for item in items:
            if item.almacenado:
                cont = cont + 1
    except TypeError:
        cont = 1
    cont = cont/5
    cont = math.ceil(cont)
    for i in range(1, cont+1):
        url = 'http://localhost:1234/?page=' + str(i)
        with urllib.request.urlopen(url) as response:
            htmlStream = response.read().decode('utf8')
        soup = BeautifulSoup(htmlStream, 'html.parser')
        books = soup.find_all(
        'ol')
        for book in books:
            link = 'http://localhost:1234/' + book.find('a')['href']
            word = book.find('input', attrs={'name': 'name'})['value']
            token = book.find('input', attrs={'name':'csrfmiddlewaretoken'})['value']
            data = {'book no': str(book_no), 'title': word, 'link': link,
                'token': token}
            book_no += 1
            res.append(data)
    link, action, verify, id = taskbar(request)
    cont_pags, pag_ej = word_recount()
    json = True
    return (render(request, 'pages/contenido_jsonorxml.html', {
                                                            'link': link,
                                                            'id': id,
                                                            'accion': action,
                                                            'autentificacion': verify,
                                                            'datos': res,
                                                            'cont_pags': cont_pags,
                                                            'pag_ej': pag_ej,
                                                            'json': json}))


## Apartado para Inicio
@csrf_exempt
def index(request):
    if request.method == 'POST':
        if 'name' in request.POST:
            if request.POST.get('select'):
                    change_word(request=request, name=request.POST['name'], selected=True)
            elif request.POST.get('deselect'):
                    change_word(request=request, name=request.POST['name'], selected=False)
        else:
            form = BuscadorForm(request.POST)
            if form.is_valid():
                print(form.cleaned_data['buscador'])
                return redirect("/" + form.cleaned_data['buscador'])

    if request.method == 'GET' and request.GET:
        format = request.GET.get("format")
        print(format)
        if format == 'xml':
            return content_xml(request)
        elif format == 'json':
            return content_json(request)
    vote_list, content_form, item_list, link, id, action, verify = load_taskbar(request)
    cont_pags, pag_ej = word_recount()
    if verify:
        load_voto(request)
    begin = True
    return(render(request, 'pages/seleccionados.html', {'vote_list' : vote_list,
                                                'item_list': item_list,
                                                'link': link,
                                                'id': id,
                                                'accion': action,
                                                'form': content_form,
                                                'autentificacion': verify,
                                                'cont_pags': cont_pags,
                                                'pag_ej': pag_ej,
                                                'inicio': begin}))


## Apartado modificación urls y comentarios
@csrf_exempt
def save_comment(request, name):
    user = Usuario.objects.get(id=request.user.id)
    p = Page.objects.get(name=name)
    form = ContentForm(request.POST)
    date = datetime.datetime.now()
    if form.is_valid():
        c = Comentario(word=p, author=user, comment=form.cleaned_data['content'], date=date)
        c.save()


@csrf_exempt
def save_url(request, name):
    user = Usuario.objects.get(id=request.user.id)
    p = Page.objects.get(name=name)
    form = BuscadorForm(request.POST)
    date = datetime.datetime.now()
    if form.is_valid():
        u = Url(word=p, author=user, url=form.cleaned_data['buscador'], date=date)
        u.save()

def load_comments(palabra):
    try:
        coms = Comentario.objects.filter(word=palabra)
        comments = []
        for com in coms:
            message = com.author.name + ': ' + com.comment + '.'
            comments.append(message)
    except Comentario.DoesNotExist:
        message = 'No hay comentarios.'
        comments = list(message)
    return comments

def load_urls(palabra):
    try:
        links = Url.objects.filter(word=palabra)
        urls = []
        for link in links:
            message = link.author.name + ': ' + link.url + '. '
            download_url_emb(palabra.name, link)
            if link.info != '':
                message = message  + 'Definicion: ' + link.info
            urls.append(message)
    except Comentario.DoesNotExist:
        message = 'No hay urls.'
        urls = list(message)
    return urls

@csrf_exempt
def word_content(request, name):
    if request.method == 'POST':
        if 'name' in request.POST:
            if request.POST.get('select'):
                    change_word(request=request, name=request.POST['name'], selected=True)
            elif request.POST.get('deselect'):
                    change_word(request=request, name=request.POST['name'], selected=False)
            elif request.POST.get('almacenar'):
                    p = Page.objects.get(name=name)
                    p.date = datetime.datetime.now()
                    p.almacenado = True
                    p.save()
        elif 'RaeDef' in request.POST:
            download_rae_def(name)
            p = Page.objects.get(name=name)
            p.rae_alm = True
            p.save()
        elif 'Flickr' in request.POST:
            p = Page.objects.get(name=name)
            p.fl_alm = True
            p.save()
        elif 'Meme' in request.POST:
            if request.POST.get('opcion1'):
                opt = 1
            elif request.POST.get('opcion2'):
                opt = 2
            elif request.POST.get('opcion3'):
                opt = 3
            download_meme(name, opt)
            p = Page.objects.get(name=name)
            p.mem_alm = True
            p.save()
        elif 'comment' in request.POST:
            save_comment(request, name)
        elif 'url' in request.POST:
            save_url(request, name)

    if request.method == 'GET' or request.method == 'POST':
        try:
            p = Page.objects.get(name=name)
            content_form = ContentForm(initial={'content': ''})
            url_form = BuscadorForm()
            status = 200
        except Page.DoesNotExist:
            try:
                if name != "favicon.ico":
                    download_word(request, name)
                    p = Page.objects.get(name=name)
                    content_form = ContentForm(initial={'content': ''})
                    url_form = BuscadorForm()
                    status = 200
                else:
                    return redirect("/")
            except Usuario.DoesNotExist:
                return redirect("/")
        content_template = loader.get_template('pages/content.html')
        source, height, width, res_wiki = download_image_wiki(name)
        comments = load_comments(p)
        urls = load_urls(p)
        link, action, verify, id = taskbar(request)
        if verify:
            load_voto(request)
        if p.almacenado:
            sec_action = ''
            inp_type ='hidden'
            if p.fl_alm:
                source2 = download_image_flickr(name)
            else:
                source2 = ''
        else:
            source2 = ''
            sec_action = 'Almacenar'
            inp_type = 'submit'
        if p.selected:
            action_voto = 'Votado'
            select = 'deselect'
        else:
            action_voto = 'Votar'
            select = 'select'
        cont_pags, pag_ej = word_recount()
        voto_count = voto_recount(p)
        if p.author != None:
            author = p.author.name
        else:
            author = None
        content_html = content_template.render({'page': name,
                                                'wik_def': p.content,
                                                'source': source,
                                                'height': height,
                                                'width': width,
                                                'source2': source2,
                                                'definicion': p.def_rae,
                                                'name': name,
                                                'res_wiki': res_wiki,
                                                'select': select,
                                                'select_value': p.selected,
                                                'accion_voto': action_voto,
                                                'seg_accion': sec_action,
                                                'inp_type': inp_type,
                                                'almacenado': p.almacenado,
                                                'content_form': content_form,
                                                'url_form': url_form,
                                                'comentarios': comments,
                                                'urls': urls,
                                                'rae_act': p.rae_alm,
                                                'fl_act':   p.fl_alm,
                                                'mem_act': p.mem_alm,
                                                'cont_pags': cont_pags,
                                                'pag_ej': pag_ej,
                                                'link': link,
                                                'id': id,
                                                'accion': action,
                                                'autentificacion': verify,
                                                'voto_count': voto_count,
                                                'author': author},
                                               request)
        return (HttpResponse(content_html, status=status))


## Apartado mi página y página ayuda
def my_page(request):
    user = Usuario.objects.get(id=request.user.id)
    pages = Page.objects.filter(author=user)
    coms = Comentario.objects.filter(author=user)
    urls = Url.objects.filter(author=user)
    list_user = []
    for page in pages:
        if page.almacenado:
            list_user.append(page)
    for com in coms:
        list_user.append(com)
    for url in urls:
        list_user.append(url)
    date_list = []
    for item in list_user:
        date_list.append(item.date)
    date_list.sort(reverse=True)
    list_user = []
    for date in date_list:
        pages = Page.objects.filter(date=date)
        coms = Comentario.objects.filter(date=date)
        urls = Url.objects.filter(date=date)
        if len(pages) == 1:
            message = str(date) + '      Palabra: ' + pages[0].name + ' --> ' + pages[0].content
            list_user.append(message)
        elif len(coms) == 1:
            message = str(date) + '      Comentario: ' + coms[0].comment + '.'
            list_user.append(message)
        elif len(urls) == 1:
            if urls[0].info != '' and urls[0].link_image != '':
                message = str(date) + '      Enlace: '+ urls[0].url + '. Información: ' + urls[0].info \
                + ' Link imagen: ' + urls[0].link_image
            elif urls[0].info != '':
                message = str(date) + '      Enlace: ' + urls[0].url + '. Información: ' + urls[0].info
            elif urls[0].link_image != '':
                message = str(date) + '      Enlace: ' + urls[0].url + '. Link imagen: ' + urls[0].link_image
            else:
                message = str(date) + '      Enlace: ' + urls[0].url
            list_user.append(message)
    link, action, verify, id = taskbar(request)
    mypage = True
    return (render(request, 'pages/mi_pagina.html', {
        'link': link,
        'id': id,
        'accion': action,
        'autentificacion': verify,
        'author': user.name,
        'list_user': list_user,
        'mipagina': mypage}))

def help(request):
    link, action, verify, id = taskbar(request)
    help = True
    return (render(request, 'pages/ayuda.html', {
        'link': link,
        'id': id,
        'accion': action,
        'autentificacion': verify,
        'ayuda': help}))


## Apartado login y logout
@csrf_exempt
def login_view(request):
    if request.method == 'POST':
            username = request.POST['nombre']
            password = request.POST['contrasena']
            user = authenticate(username=username, password=password)
            if user is not None:
                if user.is_active:
                    login(request, user)
                    try:
                        user = Usuario.object.get(name=request.POST['nombre'])
                        user.save()
                    except:
                        user = Usuario(name=request.POST['nombre'])
                        user.save()
                    return redirect("/ayuda/")
                else:
                    error = 'Usuario o contraseña mal pasados.'
                    login_ok = True
                    return (HttpResponse(render(request, 'pages/login.html', {'error': error, 'login': login_ok}), status=400))
            else:
                error = 'Usuario o contraseña mal pasados.'
                login_ok = True
                return (HttpResponse(render(request, 'pages/login.html', {'error': error, 'login': login_ok}), status=400))

    if request.method == 'GET':
        error = ''
        login_ok = True
        return (HttpResponse(render(request, 'pages/login.html',{'error': error, 'login': login_ok}), status=200))


def logout_view(request):
    logout(request)
    return redirect("/")

